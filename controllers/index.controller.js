
require('dotenv').config();
const nodemailer = require('nodemailer');
const request = require('request');

exports.renderHomepage = (req, res) => {
    res.render('index', { title: 'Raymond De Castro | Portfolio' });
}

exports.sendMessage = (req, res) => {
    if (req.body.captcha === undefined || req.body.captcha === '' || req.body.captcha === null) {
        return res.json({
            "success": false,
            "msg":"Please verify captcha."
        });
    }

    const secretKey = process.env.RECAPTCHA_SECRET_KEY;

    // Verify Url
    const verifyUrl = `https://google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${req.body.captcha}&remoteip=${req.connection.remoteAddress}`;

    request(verifyUrl, (err, response, body) => {
        body = JSON.parse(body);

        // If not successful
        if (body.success !== undefined && !body.success){
            return res.json({
                "success": false,
                "msg":"Failed captcha verificaton."
            });
        }

        let transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
            port: 465,
            secure: true,
            auth: {
                user: process.env.SMTP_USERNAME,
                pass: process.env.SMTP_PASSWORD
            }
        });

        message = {
            from: 'Portfolio Mailer <raymond@decastro.ph>',
            replyTo: req.body.email,
            to: process.env.MESSAGE_DESTINATION,
            subject: "Portfolio Message Received!",
            html: `<b>Name</b>: ${req.body.name}<br>
            <b>Message</b>: ${req.body.message}`
        }

        transporter.sendMail(message, (err) => {
            if (err) {
                console.error(err)
            } else {
                console.log('Message successfully sent!')
            }
        });

        // If successful
        return res.json({
            "success": true,
            "msg":"Successfully verified captcha."
        });
    });
}